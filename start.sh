#!/bin/bash

set -eu -o pipefail

echo "=> Create sogo.conf"
[[ ! -f /app/data/sogo.conf ]] && cp /app/code/sogo.conf.template /app/data/sogo.conf

sed -e "s,SOGoProfileURL =.*,SOGoProfileURL = \"${CLOUDRON_MYSQL_URL}/sogo_user_profile\";," \
    -e "s,OCSFolderInfoURL =.*,OCSFolderInfoURL = \"${CLOUDRON_MYSQL_URL}/sogo_folder_info\";," \
    -e "s,OCSSessionsFolderURL =.*,OCSSessionsFolderURL = \"${CLOUDRON_MYSQL_URL}/sogo_sessions_folder\";," \
    -e "s,OCSEMailAlarmsFolderURL =.*,OCSEMailAlarmsFolderURL = \"${CLOUDRON_MYSQL_URL}/sogo_alarms_folder\";," \
    -e "s,SOGoIMAPServer =.*,SOGoIMAPServer = \"imap://${CLOUDRON_EMAIL_IMAP_SERVER}:9393\";," \
    -e "s,SOGoSMTPServer =.*,SOGoSMTPServer = ${CLOUDRON_EMAIL_SMTP_SERVER}:${CLOUDRON_EMAIL_SMTP_PORT};," \
    -e "s,SOGoSieveServer =.*,SOGoSieveServer = \"sieve://${CLOUDRON_EMAIL_SERVER_HOST}:4190/?tls=YES\";," \
    -e "s!baseDN =.*!baseDN = \"${CLOUDRON_EMAIL_LDAP_MAILBOXES_BASE_DN}\";!" \
    -e "s!bindDN =.*!bindDN = \"${CLOUDRON_LDAP_BIND_DN}\";!" \
    -e "s,bindPassword =.*,bindPassword = \"${CLOUDRON_LDAP_BIND_PASSWORD}\";," \
    -e "s,hostname =.*,hostname = ${CLOUDRON_LDAP_URL};," \
    -i /app/data/sogo.conf

# migration, remove in next release
sed -e 's,WOLogFile = .*;,WOLogFile = "-";,' \
    -e 's,WOPidFile = .*;,WOPidFile = "/run/sogo/sogo.pid";,' \
    -i /app/data/sogo.conf

echo "=> Ensure directories"
mkdir -p /run/GNUstep /run/nginx /run/sogo

echo "=> Generating nginx.conf"
sed -e "s,##HOSTNAME##,${CLOUDRON_APP_DOMAIN}," /app/code/nginx.conf.template  > /run/nginx/nginx.conf

# sogo has explicit default of 1 if the value is missing. so it's actually enabled if config is missing
worker_count=$(sed -ne 's/.*WOWorkersCount = \(.*\);/\1/p' sogo.conf.template)
if [[ $worker_count -eq 0 ]]; then
    echo "=> ActiveSync support disabled"
    sed -e '/## Begin ActiveSync/,/## End ActiveSync/d' -i /run/nginx/nginx.conf
else
    echo "=> ActiveSync support enabled"
fi

echo "=> Changing ownership"
mkdir -p /app/data/spool
chown -R cloudron:cloudron /run/{GNUstep,nginx,sogo} /app/data

echo "=> Starting SOGo"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i SOGo
