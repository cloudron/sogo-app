FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/build/sope /app/code/build/sogo /app/code/build/wbxml2 /tmp/wbxml2

RUN apt-get update && \
    apt-get install -y gobjc libgnustep-base-dev gnustep-base-common gnustep-make libldap2-dev libssl-dev libpq-dev libwbxml2-dev libmemcached-dev memcached liblzma-dev libzip-dev liboath-dev libytnef0-dev && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-releases depName=Alinto/sogo versioning=semver extractVersion=^SOGo-(?<version>.+)$
ARG SOGO_VERSION=5.11.2
ARG SOPE_VERSION=${SOGO_VERSION}

# https://github.com/libwbxml/libwbxml/releases
# renovate: datasource=github-releases depName=libwbxml/libwbxml versioning=semver extractVersion=^libwbxml-(?<version>.+)$
ARG WBXML_VERSION=0.11.10

WORKDIR /app/code/build/sope
RUN curl -SLf "https://github.com/Alinto/sope/archive/SOPE-${SOPE_VERSION}.tar.gz" | tar -zx --strip-components 1 -f - -C /app/code/build/sope && \
    ./configure --with-gnustep && make && make install && \
    rm -rf /app/code/build/sope

WORKDIR /app/code/build/sogo
RUN curl -SLf "https://github.com/Alinto/sogo/archive/SOGo-${SOGO_VERSION}.tar.gz" | tar -zx --strip-components 1 -f - -C /app/code/build/sogo && \
    ./configure --enable-mfa && make && make install

WORKDIR /tmp/wbxml2
RUN curl -SLf "https://github.com/libwbxml/libwbxml/archive/libwbxml-${WBXML_VERSION}.tar.gz" | tar -zx --strip-components 1 -f - -C /tmp/wbxml2 && \
    cmake . -B/app/code/build/wbxml2

WORKDIR /app/code/build/wbxml2
RUN make && make install && \
    echo "/usr/local/lib" > /etc/ld.so.conf.d/local.conf && \
    ldconfig && \
    ln -s /usr/local/include/libwbxml-1.0/wbxml /usr/include

# Building ActiveSync
WORKDIR /app/code/build/sogo/ActiveSync
RUN make && make install

RUN rm -rf /tmp/wbxml2 /app/code/build/wbxml2 /app/code/build/sogo

# Register SOGo library
RUN echo "/usr/local/lib/sogo" > /etc/ld.so.conf.d/sogo.conf && \
    ldconfig

RUN mkdir -p /etc/sogo && \
    rm -rf /etc/sogo/sogo.conf && ln -s /app/data/sogo.conf /etc/sogo/sogo.conf && \
    rm -rf /var/log/nginx && ln -s /run/nginx /var/log/nginx && \
    mkdir /run/GNUstep && ln -s /run/GNUstep /home/cloudron/GNUstep

# configure supervisor
RUN crudini --set /etc/supervisor/supervisord.conf supervisord logfile /run/supervisord.log && \
	crudini --set /etc/supervisor/supervisord.conf supervisord logfile_backups 0
ADD supervisor/ /etc/supervisor/conf.d/

COPY sogo.conf.template nginx.conf.template start.sh /app/code/

RUN chown -R cloudron:cloudron /etc/sogo
# Create the spool directory on /run and symlink the original one
RUN rm -rf /var/spool/sogo && mkdir -p /app/data/spool && ln -s /app/data/spool /var/spool/sogo

ENV LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib/sogo

WORKDIR /app/code
CMD [ "/app/code/start.sh" ]
